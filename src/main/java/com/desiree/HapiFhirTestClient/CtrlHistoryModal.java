package com.desiree.HapiFhirTestClient;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.Button;
import org.zkoss.zul.Checkbox;
import org.zkoss.zul.Column;
import org.zkoss.zul.Columns;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Div;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Row;
import org.zkoss.zul.Rows;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

import com.desiree.model.PatientHistory3;

@SuppressWarnings("rawtypes")
public class CtrlHistoryModal extends GenericForwardComposer {

	private static final long serialVersionUID = 1L;

	private Window modalHistory;
	private Label modalTitulo;
	private int ultimaFila=0;
	private ListModelList listLocationModel = new ListModelList(Arrays.asList(new String[] { "Breast", "Ovario", "Other" }));
	private ListModelList listRelatives = new ListModelList(Arrays.asList(new String[] { "Mother", "Maternal grandmother", "Paternal grandmother", "Sister", "Daughter", "Collateral family", "Father", "Brother", "Grandfather"}));	
	private PatientHistory3 ph3=new PatientHistory3();
	private Div divAddEdit;
	private Columns columnsModalHistory;
	private Rows rowsModalHistory;
	private Button btnAddModalHistory;
	private boolean editando=false;
	private int filaEditada=0;
	
	@SuppressWarnings("unchecked")
	public void doAfterCompose(Component comp) throws Exception {
		super.doAfterCompose(comp);
		
		Map<Integer , Object> map=new HashMap<Integer,Object>();
        map=(Map<Integer, Object>) Executions.getCurrent().getArg();
        if(map.get(0)!=null){
//        	origen=(String)map.get(0);
//        	System.out.println("Origen: "+origen);
//        	if(map.get(1)==null){
//        		sinDatos=true;
//        	}else{
//        		sinDatos=false;
//        	}
        }
        crearCampos();
		pintarModalHistory(map);
	}
	
	private void pintarModalHistory(Map<Integer, Object> map){
		
		Column col1=new Column();
		Column col2=new Column();
		Column col3=new Column();
		Column col4=new Column();
		modalTitulo.setValue("Edit family cancer location");
		col1.setLabel("LOCATION");
		col1.setWidth("200px");
		columnsModalHistory.appendChild(col1);
		col2.setLabel("RELATIVE");
		col2.setWidth("200px");
		columnsModalHistory.appendChild(col2);
		col3.setLabel("AGE");
		col3.setWidth("100px");
		col3.setAlign("right");
		columnsModalHistory.appendChild(col3);
		columnsModalHistory.appendChild(col4);
		
		if(map.get(1)==null){
			modalTitulo.setValue("Add family cancer location");
			
		}else{
			
			Iterator entries;
			entries = map.entrySet().iterator();
			while (entries.hasNext()) {
			    Map.Entry entry = (Map.Entry) entries.next();
			    Integer key = (Integer)entry.getKey();
				ultimaFila=key;
		    	if(entry.getValue().getClass().equals(PatientHistory3.class)){
				    ph3 = (PatientHistory3)entry.getValue();
				    agregarFila3(ph3);
			    }
			}	
		}
	}
	
	
	
	/**
	 * Crea el grid en la modal para la modal3
	 */
	private void agregarFila3(PatientHistory3 ph3){
		Row row=new Row();
		row.setId("row_"+ultimaFila);
		row.setClass("sinFondo");
		
		Label lblLocation = new Label();				
		lblLocation.setValue(ph3.getCancerLocation());			
		lblLocation.setId("lblLocation_"+ultimaFila);
		row.appendChild(lblLocation);
		
		Label lblRelative= new Label();				
		lblRelative.setValue(ph3.getCancerRelative());			
		lblRelative.setId("lblRelative_"+ultimaFila);
		row.appendChild(lblRelative);
		
		Label lblAge=new Label();			
		lblAge.setValue(String.valueOf(ph3.getCancerAge()));
		lblAge.setWidth("90%");
		lblAge.setId("lblAge_"+ultimaFila);
		row.appendChild(lblAge);
		
		Div divBotonera=new Div();
		Button btnEdit=new Button();
		btnEdit.setId("btnEdit_"+ultimaFila);
		btnEdit.setClass("z-button-table");
		btnEdit.setIconSclass("z-icon-pencil");		
		divBotonera.appendChild(btnEdit);		
		Button btnDelete=new Button();
		btnDelete.setId("btnDelete_"+ultimaFila);
		btnDelete.setClass("z-button-table");
		btnDelete.setIconSclass("z-icon-times");		
		divBotonera.appendChild(btnDelete);
		row.appendChild(divBotonera);
		
		btnEdit.addEventListener("onClick", new EventListener<Event>() {
			public void onEvent(Event event) throws Exception {
				int fila=Integer.parseInt(event.getTarget().getId().split("_")[1]);
				editando=true;
				filaEditada=fila;
				btnAddModalHistory.setVisible(false);
				divAddEdit.getParent().setVisible(true);
				if(divAddEdit.getFellowIfAny("cmbCancerLocation")!=null){
					Combobox cmbCancerLocation=(Combobox)divAddEdit.getFellowIfAny("cmbCancerLocation");
					cmbCancerLocation.setValue(((Label)divAddEdit.getFellowIfAny("lblLocation_"+fila)).getValue());
					String location=((Label)divAddEdit.getFellowIfAny("lblLocation_"+fila)).getValue();
					if(!listLocationModel.contains(location)){
						Textbox txtOther=((Textbox)divAddEdit.getFellowIfAny("txtCancerLocation"));
						txtOther.setValue(location);
						txtOther.setVisible(true);
						cmbCancerLocation.setValue("Other");
					}else{
						cmbCancerLocation.setValue(location);
					}					
				}
				if(divAddEdit.getFellowIfAny("cmbRelative")!=null){
					Combobox cmbRelative=(Combobox)divAddEdit.getFellowIfAny("cmbRelative");
					cmbRelative.setValue(((Label)divAddEdit.getFellowIfAny("lblRelative_"+fila)).getValue());
				}					
				if(divAddEdit.getFellowIfAny("txtAge")!=null){
					Textbox txtAge=(Textbox)divAddEdit.getFellowIfAny("txtAge");
					txtAge.setVisible(true);
					txtAge.setValue(((Label)divAddEdit.getFellowIfAny("lblAge_"+fila)).getValue());
				}
				if(divAddEdit.getFellowIfAny("txtCancerLocation")!=null){
					Textbox txtOther=((Textbox)divAddEdit.getFellowIfAny("txtCancerLocation"));
					txtOther.setValue(((Label)divAddEdit.getFellowIfAny("lblLocation_"+fila)).getValue());
				}
				
			}			
		});
		
		btnDelete.addEventListener("onClick", new EventListener<Event>() {
			public void onEvent(Event event) throws Exception {
				int fila=Integer.parseInt(event.getTarget().getId().split("_")[1]);
				rowsModalHistory.getFellowIfAny("row_"+fila).detach();
			}
		});

		rowsModalHistory.appendChild(row);	
	}
	
	
	
	public void onClick$btnAcceptModalHistory(){
		Map<Integer, Object> mapa = new HashMap<Integer, Object>();
		for(int i=1;i<=ultimaFila;i++){
			PatientHistory3 ph3 = new PatientHistory3();
			if(rowsModalHistory.getFellowIfAny("lblLocation_"+i)!=null && rowsModalHistory.getFellowIfAny("lblRelative_"+i)!=null && rowsModalHistory.getFellowIfAny("lblAge_"+i)!=null){
				ph3.setCancerLocation(((Label)rowsModalHistory.getFellowIfAny("lblLocation_"+i)).getValue());
				ph3.setCancerRelative(((Label)rowsModalHistory.getFellowIfAny("lblRelative_"+i)).getValue());
				ph3.setCancerAge(Integer.parseInt(((Label)rowsModalHistory.getFellowIfAny("lblAge_"+i)).getValue()));
				mapa.put(i-1, ph3);
			}
		}
		Events.postEvent("onClose", modalHistory, mapa);
	}
	
	public void onClick$btnCancelModalHistory(){			
		modalHistory.detach();
	}
	
	public void onClick$btnAddModalHistory(){
		btnAddModalHistory.setVisible(false);
		divAddEdit.getParent().setVisible(true);
	}

	@SuppressWarnings("unchecked")
	private void crearCampos(){
		Label lbl=new Label();
		Combobox cmbCancerLocation=new Combobox();		
		final Textbox txtAge=new Textbox();
		final Textbox txtOther=new Textbox();
		lbl.setValue("Insert cancer location, relative and age");
		lbl.setClass("etiquetasL16 salto");
		divAddEdit.appendChild(lbl);
	
		cmbCancerLocation.setModel(listLocationModel);
		cmbCancerLocation.setClass("combo");
		cmbCancerLocation.setId("cmbCancerLocation");
		divAddEdit.appendChild(cmbCancerLocation);

		txtOther.setId("txtCancerLocation");
		txtOther.setVisible(false);
		divAddEdit.appendChild(txtOther);

		final Combobox cmbRelative=new Combobox();
		cmbRelative.setModel(listRelatives);
		cmbRelative.setClass("combo");
		cmbRelative.setId("cmbRelative");
		divAddEdit.appendChild(cmbRelative);
		
		txtAge.setId("txtAge");
		txtAge.setClass("textbox-num");
		divAddEdit.appendChild(txtAge);
		
		cmbCancerLocation.addEventListener("onSelect", new EventListener() {
			public void onEvent(Event e) throws Exception {
				if("Other".equalsIgnoreCase(((Combobox) e.getTarget()).getValue())){
					txtOther.setVisible(true);
					txtOther.setFocus(true);
				}else{
					txtOther.setVisible(false);
					cmbRelative.setFocus(true);
				}
			}
		});
	}
	/**
	 * M�todo que a�ade/edita los datos de la capa al grid de la modal
	 */
	public void onClick$btnOk(){
		
		Combobox cbLocation = (Combobox) divAddEdit.getFellowIfAny("cmbCancerLocation");
		Textbox txtLocation = (Textbox) divAddEdit.getFellowIfAny("txtCancerLocation");
		Combobox cbrelative = (Combobox) divAddEdit.getFellowIfAny("cmbRelative");
		Textbox txtAge = (Textbox) divAddEdit.getFellowIfAny("txtAge");
		if(cbLocation.getSelectedItem()!=null && cbrelative.getSelectedItem()!=null && txtAge.getValue()!=null && !txtAge.getValue().isEmpty()){
			divAddEdit.getParent().setVisible(false);
			
			if(editando){
				Label lblLocation= new Label();
				Label lblAge= new Label();
				lblLocation=(Label)rowsModalHistory.getFellowIfAny("lblLocation_"+filaEditada);
				if("Other".equalsIgnoreCase(((Combobox)divAddEdit.getFellow("cmbCancerLocation")).getValue()) && (divAddEdit.getFellowIfAny("txtCancerLocation")!=null)){
					lblLocation.setValue(((Textbox) divAddEdit.getFellowIfAny("txtCancerLocation")).getValue());						
				}else{
					lblLocation.setValue(((Combobox)divAddEdit.getFellow("cmbCancerLocation")).getValue());
				}	
//				lblRelative=(Label)rowsModalHistory.getFellowIfAny("lblRelative_"+filaEditada);
				lblAge=(Label) rowsModalHistory.getFellowIfAny("lblAge_"+filaEditada);
				lblAge.setValue(((Textbox)divAddEdit.getFellow("txtAge")).getValue());
			}else{
				ultimaFila++;
				Row row=new Row();
				row.setId("row_"+ultimaFila);
				row.setClass("sinFondo");
				Label lblLocation= new Label();
				lblLocation.setId("lblLocation_"+ultimaFila);
				Div divBotonera=new Div();
				Button btnEdit=new Button();
				Label lblAge= new Label();
				lblAge.setId("lblAge_"+ultimaFila);
				if(divAddEdit.getFellowIfAny("txtAge")!=null)
					lblAge.setValue(((Textbox)divAddEdit.getFellow("txtAge")).getValue());
				btnEdit.setId("btnEdit_"+ultimaFila);
				btnEdit.setClass("z-button-table");
				btnEdit.setIconSclass("z-icon-pencil");		
				divBotonera.appendChild(btnEdit);		
				Button btnDelete=new Button();
				btnDelete.setId("btnDelete_"+ultimaFila);
				btnDelete.setClass("z-button-table");
				btnDelete.setIconSclass("z-icon-times");		
				divBotonera.appendChild(btnDelete);

				Label lblRelative =  new Label();
				if("Other".equalsIgnoreCase(((Combobox)divAddEdit.getFellow("cmbCancerLocation")).getValue()) && (divAddEdit.getFellowIfAny("txtCancerLocation")!=null)){
					lblLocation.setValue(((Textbox) divAddEdit.getFellowIfAny("txtCancerLocation")).getValue());						
				}else{
					lblLocation.setValue(((Combobox)divAddEdit.getFellow("cmbCancerLocation")).getValue());
				}
				row.appendChild(lblLocation);
				
				lblRelative.setId("lblRelative_"+ultimaFila);
				lblRelative.setValue(((Combobox)divAddEdit.getFellow("cmbRelative")).getValue());
				row.appendChild(lblRelative);
				
				row.appendChild(lblAge);
			
				row.appendChild(divBotonera);
				
				btnEdit.addEventListener("onClick", new EventListener<Event>() {
					public void onEvent(Event event) throws Exception {
						int fila=Integer.parseInt(event.getTarget().getId().split("_")[1]);
						editando=true;
						filaEditada=fila;
						btnAddModalHistory.setVisible(false);
						divAddEdit.getParent().setVisible(true);
						if(divAddEdit.getFellowIfAny("cmbCancerLocation")!=null){
							Combobox cmbCancerLocation=(Combobox)divAddEdit.getFellowIfAny("cmbCancerLocation");
							String location=((Label)divAddEdit.getFellowIfAny("lblLocation_"+fila)).getValue();
							if(!listLocationModel.contains(location)){
								Textbox txtOther=((Textbox)divAddEdit.getFellowIfAny("txtCancerLocation"));
								txtOther.setValue(location);
								txtOther.setVisible(true);
								cmbCancerLocation.setValue("Other");
							}else{
								cmbCancerLocation.setValue(location);
							}
							
						}
						if(divAddEdit.getFellowIfAny("cmbRelative")!=null){
							Combobox cmbRelative=(Combobox)divAddEdit.getFellowIfAny("cmbRelative");
							cmbRelative.setValue(((Label)divAddEdit.getFellowIfAny("lblRelative_"+fila)).getValue());
						}
						if(divAddEdit.getFellowIfAny("txtAge")!=null){
							Textbox txtAge=(Textbox)divAddEdit.getFellowIfAny("txtAge");
							txtAge.setValue(((Label)divAddEdit.getFellowIfAny("lblAge_"+fila)).getValue());
						}
						if(divAddEdit.getFellowIfAny("txtCancerLocation")!=null){
							Textbox txtOther=((Textbox)divAddEdit.getFellowIfAny("txtCancerLocation"));
							txtOther.setValue(((Label)divAddEdit.getFellowIfAny("lblLocation_"+fila)).getValue());
						}
					}			
				});
				
				btnDelete.addEventListener("onClick", new EventListener<Event>() {
					public void onEvent(Event event) throws Exception {
						int fila=Integer.parseInt(event.getTarget().getId().split("_")[1]);
						rowsModalHistory.getFellowIfAny("row_"+fila).detach();
					}
				});
				
				rowsModalHistory.appendChild(row);
			}
			btnAddModalHistory.setVisible(true);
//			sinDatos=false;
			limpiarCampos();
			editando=false;
			
			
		}else{
			Clients.showNotification("You must introduce values for the fields: location, relative and age", true);
		}
		
		
	}
	
	
	/**
	 * M�todo que cancela la edici�n/adici�n de filas
	 */
	public void onClick$btnCancel(){
		divAddEdit.getParent().setVisible(false);
		limpiarCampos();
	}
	
	private void limpiarCampos(){
		btnAddModalHistory.setVisible(true);
		if(divAddEdit.getFellowIfAny("cmbCancerLocation")!=null){
			((Combobox)divAddEdit.getFellowIfAny("cmbCancerLocation")).setValue("");
		}
		if(divAddEdit.getFellowIfAny("txtAge")!=null){
			((Textbox)divAddEdit.getFellowIfAny("txtAge")).setValue("");		
		}
		if(divAddEdit.getFellowIfAny("txtCancerLocation")!=null){
			((Textbox)divAddEdit.getFellowIfAny("txtCancerLocation")).setValue("");	
			((Textbox)divAddEdit.getFellowIfAny("txtCancerLocation")).setVisible(false);
		}
		if(divAddEdit.getFellowIfAny("cmbDiseaseLocation")!=null){
			((Combobox)divAddEdit.getFellowIfAny("cmbDiseaseLocation")).setValue("");			
		}
		if(divAddEdit.getFellowIfAny("chkLeft")!=null){
			((Checkbox)divAddEdit.getFellowIfAny("chkLeft")).setChecked(false);
		}
		if(divAddEdit.getFellowIfAny("chkRight")!=null){
			((Checkbox)divAddEdit.getFellowIfAny("chkRight")).setChecked(false);
		}
		if(divAddEdit.getFellowIfAny("cmbRelative")!=null){
			((Combobox)divAddEdit.getFellowIfAny("cmbRelative")).setValue("");
		}
		if(divAddEdit.getFellowIfAny("cmbUnit")!=null){
			((Combobox)divAddEdit.getFellowIfAny("cmbUnit")).setValue("");
		}
		if(divAddEdit.getFellowIfAny("txtAllergy")!=null){
			((Textbox)divAddEdit.getFellowIfAny("txtAllergy")).setValue("");		
		}
		if(divAddEdit.getFellowIfAny("txtMonthsYears")!=null){
			((Textbox)divAddEdit.getFellowIfAny("txtMonthsYears")).setValue("");		
		}
		
		
	}
	
}



