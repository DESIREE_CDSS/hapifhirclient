package com.desiree.HapiFhirTestClient;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.zkoss.lang.Library;
import org.zkoss.web.Attributes;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Execution;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.Checkbox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Rows;
import org.zkoss.zul.Window;

import com.desiree.model.HistoryRiskFactors;

@SuppressWarnings("rawtypes")
public class CtrlRiskFactors extends GenericForwardComposer {

	private static final long serialVersionUID = 1L;

	private String locale = "";
	private Window modalRiskFactors;
	private Component comp;
	private Rows rowsOtherRiskFactors;
	private Label modalTitulo;
	private int ultimaFila=0;
	private Checkbox brca1Y,brca1N,brca2Y,brca2N,palb2Y,palb2N,rad51cY,rad51cN,rad51dY,rad51dN,p53Y,p53N,ptenY,ptenN,stk11Y,stk11N,ihcY,ihcN,atypicalY,atypicalN;
	
	@SuppressWarnings("unchecked")
	public void doAfterCompose(Component comp) throws Exception {

		
		super.doAfterCompose(comp);
		locale = Library.getProperty(Attributes.PREFERRED_LOCALE);
		this.comp=comp;

		Execution exec = Executions.getCurrent();
		Map map = exec.getArg();
		pintarModalHistory(map);
	
	}
	
	@SuppressWarnings("unchecked")
	private void pintarModalHistory(Map<Integer, HistoryRiskFactors> map){
		HistoryRiskFactors hrf=new HistoryRiskFactors();
		if(map.get(0)!=null){
			hrf=map.get(0);
		    if(hrf.getBrca1()!=null)
		    	if(hrf.getBrca1()) brca1Y.setChecked(true);
		    	else brca1N.setChecked(true);
		    if(hrf.getPalb2()!=null)
		    	if(hrf.getPalb2()) palb2Y.setChecked(true);
		    	else palb2N.setChecked(true);
		    if(hrf.getAtypicalHiperplaxia()!=null)
		    	if(hrf.getAtypicalHiperplaxia()) atypicalY.setChecked(true);
		    	else atypicalN.setChecked(true);
		    if(hrf.getPalb2()!=null)
		    	if(hrf.getPalb2()) palb2Y.setChecked(true);
		    	else palb2N.setChecked(true);
		    if(hrf.getRad51c()!=null)
		    	if(hrf.getRad51c()) rad51cY.setChecked(true);
		    	else rad51cN.setChecked(true);
		    if(hrf.getRad51d()!=null)
		    	if(hrf.getRad51d()) rad51dY.setChecked(true);
		    	else rad51dN.setChecked(true);
		    if(hrf.getP53()!=null)
		    	if(hrf.getP53()) p53Y.setChecked(true);
		    	else p53N.setChecked(true);
		    if(hrf.getPten()!=null)
		    	if(hrf.getPten()) ptenY.setChecked(true);
		    	else ptenN.setChecked(true);
		    if(hrf.getStk11()!=null)
		    	if(hrf.getStk11()) stk11Y.setChecked(true);
		    	else stk11N.setChecked(true);
		    if(hrf.getIhc()!=null)
		    	if(hrf.getIhc()) ihcY.setChecked(true);
		    	else ihcN.setChecked(true);
		}
	}
	
	@SuppressWarnings("unchecked")
	public void onClick$btnAcceptOtherRiskFactors(){
		Map<Integer, HistoryRiskFactors> mapa = new HashMap<Integer, HistoryRiskFactors>();
		
		HistoryRiskFactors hrf = new HistoryRiskFactors();
		if(brca1Y.isChecked()) hrf.setBrca1(true);
		if(brca1N.isChecked()) hrf.setBrca1(false);
		if(brca2Y.isChecked()) hrf.setBrca2(true);
		if(brca2N.isChecked()) hrf.setBrca2(false);
		if(atypicalY.isChecked()) hrf.setAtypicalHiperplaxia(true);
		if(atypicalN.isChecked()) hrf.setAtypicalHiperplaxia(false);		
		if(palb2Y.isChecked()) hrf.setPalb2(true);
		if(palb2N.isChecked()) hrf.setPalb2(false);
		if(rad51cY.isChecked()) hrf.setRad51c(true);
		if(rad51cN.isChecked()) hrf.setRad51c(false);
		if(rad51dY.isChecked()) hrf.setRad51d(true);
		if(rad51dN.isChecked()) hrf.setRad51d(false);
		if(p53Y.isChecked()) hrf.setP53(true);
		if(p53N.isChecked()) hrf.setP53(true);
		if(ptenY.isChecked()) hrf.setPten(true);
		if(ptenN.isChecked()) hrf.setPten(false);
		if(stk11Y.isChecked()) hrf.setStk11(true);
		if(stk11N.isChecked()) hrf.setStk11(false);
		if(ihcY.isChecked()) hrf.setIhc(true);
		if(ihcN.isChecked()) hrf.setIhc(false);
		
		mapa.put(0, hrf);
		Events.postEvent("onClose", modalRiskFactors, mapa);
	}
	
	public void onClick$btnCancelOtherRiskFactors(){			
		modalRiskFactors.detach();
	}


	private void verMapa(Map<Integer, HistoryRiskFactors> map){
		System.out.println("----MAPA----");
		Iterator entries = map.entrySet().iterator();
		while (entries.hasNext()) {
		    Map.Entry entry = (Map.Entry) entries.next();
		    Integer key = (Integer)entry.getKey();
		    HistoryRiskFactors value = (HistoryRiskFactors)entry.getValue();
		    System.out.println("Key = " + key + ", Value = " + value.toString());
		}
		System.out.println("--FIN MAPA--");
	}
	
	
	/*******CHECKBOXES *************/
	
	public void onCheck$brca1Y(){
		Label label = new Label();
		label=(Label)modalRiskFactors.getFellow("brca1Y").getParent().getFirstChild();
		if(brca1Y.isChecked()){
			label.setClass("textYesColor");
			brca1N.setChecked(false);
		}else{
			label.setClass("textDefaultColor");
		}
	}
	public void onCheck$brca1N(){
		Label label = new Label();
		label=(Label)modalRiskFactors.getFellow("brca1N").getParent().getFirstChild();		
		if(brca1N.isChecked()){
			brca1Y.setChecked(false);
			label.setClass("textYesColor");
		}else{
			label.setClass("textDefaultColor");
		}
	}
	public void onCheck$brca2Y(){
		Label label = new Label();
		label=(Label)modalRiskFactors.getFellow("brca2Y").getParent().getFirstChild();
		if(brca2Y.isChecked()){
			label.setClass("textYesColor");
			brca2N.setChecked(false);
		}else{
			label.setClass("textDefaultColor");
		}
	}
	public void onCheck$brca2N(){
		Label label = new Label();
		label=(Label)modalRiskFactors.getFellow("brca2N").getParent().getFirstChild();	
		if(brca2N.isChecked()){
			brca2Y.setChecked(false);
			label.setClass("textYesColor");
		}else{
			label.setClass("textDefaultColor");			
		}
	}
	
	public void onCheck$palb2Y(){
		Label label = new Label();
		label=(Label)modalRiskFactors.getFellow("palb2Y").getParent().getFirstChild();
		if(palb2Y.isChecked()){
			label.setClass("textYesColor");
			palb2N.setChecked(false);
		}else{
			label.setClass("textDefaultColor");			
		}
	}
	public void onCheck$palb2N(){
		Label label = new Label();
		label=(Label)modalRiskFactors.getFellow("palb2N").getParent().getFirstChild();		
		if(palb2N.isChecked()){
			palb2Y.setChecked(false);
			label.setClass("textYesColor");
		}else{
			label.setClass("textDefaultColor");			
		}
	}
	public void onCheck$rad51cY(){
		Label label = new Label();
		label=(Label)modalRiskFactors.getFellow("rad51cY").getParent().getFirstChild();		
		if(rad51cY.isChecked()){
			label.setClass("textYesColor");
			rad51cN.setChecked(false);
		}else{
			label.setClass("textDefaultColor");
		}
	}
	public void onCheck$rad51cN(){
		Label label = new Label();
		label=(Label)modalRiskFactors.getFellow("rad51cN").getParent().getFirstChild();		
		if(rad51cN.isChecked()){
			rad51cY.setChecked(false);
			label.setClass("textYesColor");
		}else{
			label.setClass("textDefaultColor");
		}
	}
	public void onCheck$rad51dY(){
		Label label = new Label();
		label=(Label)modalRiskFactors.getFellow("rad51dY").getParent().getFirstChild();		
		if(rad51dY.isChecked()){
			label.setClass("textYesColor");
			rad51dN.setChecked(false);
		}else{
			label.setClass("textDefaultColor");
		}
	}
	public void onCheck$rad51dN(){
		Label label = new Label();
		label=(Label)modalRiskFactors.getFellow("rad51dN").getParent().getFirstChild();		
		if(rad51dN.isChecked()){
			rad51dY.setChecked(false);
			label.setClass("textYesColor");
		}else{
			label.setClass("textDefaultColor");
		}
	}
	public void onCheck$p53Y(){
		Label label = new Label();
		label=(Label)modalRiskFactors.getFellow("p53Y").getParent().getFirstChild();		
		if(p53Y.isChecked()){
			label.setClass("textYesColor");
			p53N.setChecked(false);
		}else{
			label.setClass("textDefaultColor");
		}
	}
	public void onCheck$p53N(){
		Label label = new Label();
		label=(Label)modalRiskFactors.getFellow("p53N").getParent().getFirstChild();		
		if(p53N.isChecked()){
			p53Y.setChecked(false);
			label.setClass("textYesColor");
		}else{
			label.setClass("textDefaultColor");
		}
	}
	public void onCheck$ptenY(){
		Label label = new Label();
		label=(Label)modalRiskFactors.getFellow("ptenY").getParent().getFirstChild();		
		if(ptenY.isChecked()){
			label.setClass("textYesColor");
			ptenN.setChecked(false);
		}else{
			label.setClass("textDefaultColor");
		}
	}
	public void onCheck$ptenN(){
		Label label = new Label();
		label=(Label)modalRiskFactors.getFellow("ptenN").getParent().getFirstChild();	
		if(ptenN.isChecked()){
			ptenY.setChecked(false);
			label.setClass("textYesColor");
		}else{
			label.setClass("textDefaultColor");
		}
	}
	public void onCheck$stk11Y(){
		Label label = new Label();
		label=(Label)modalRiskFactors.getFellow("stk11Y").getParent().getFirstChild();		
		if(stk11Y.isChecked()){
			label.setClass("textYesColor");
			stk11N.setChecked(false);
		}else{
			label.setClass("textDefaultColor");
		}
	}	
	public void onCheck$stk11N(){
		Label label = new Label();
		label=(Label)modalRiskFactors.getFellow("stk11N").getParent().getFirstChild();		
		if(stk11N.isChecked()){
			stk11Y.setChecked(false);
			label.setClass("textYesColor");
		}else{
			label.setClass("textDefaultColor");
		}
	}
	public void onCheck$ihcY(){
		Label label = new Label();
		label=(Label)modalRiskFactors.getFellow("ihcY").getParent().getFirstChild();		
		if(ihcY.isChecked()){
			label.setClass("textYesColor");
			ihcN.setChecked(false);
		}else{
			label.setClass("textDefaultColor");
		}
	}	
	public void onCheck$ihcN(){
		Label label = new Label();
		label=(Label)modalRiskFactors.getFellow("ihcN").getParent().getFirstChild();		
		if(ihcN.isChecked()){
			ihcY.setChecked(false);
			label.setClass("textYesColor");
		}else{
			label.setClass("textDefaultColor");
		}
	}	
	public void onCheck$atypicalY(){
		Label label = new Label();
		label=(Label)modalRiskFactors.getFellow("atypicalY").getParent().getFirstChild();		
		if(atypicalY.isChecked()){
			label.setClass("textYesColor");
			atypicalN.setChecked(false);
		}else{
			label.setClass("textDefaultColor");
		}
	}
	public void onCheck$atypicalN(){
		Label label = new Label();
		label=(Label)modalRiskFactors.getFellow("atypicalN").getParent().getFirstChild();		
		if(atypicalN.isChecked()){
			atypicalY.setChecked(false);
			label.setClass("textYesColor");
		}else{
			label.setClass("textDefaultColor");
		}
	}		
}



