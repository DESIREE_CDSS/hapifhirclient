package com.desiree.HapiFhirTestClient;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Grid;
import org.zkoss.zul.Label;
import org.zkoss.zul.Radiogroup;
import org.zkoss.zul.Row;
import org.zkoss.zul.Rows;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.model.dstu2.composite.CodeableConceptDt;
import ca.uhn.fhir.model.dstu2.composite.CodingDt;
import ca.uhn.fhir.model.dstu2.composite.ResourceReferenceDt;
import ca.uhn.fhir.model.dstu2.resource.Bundle;
import ca.uhn.fhir.model.dstu2.resource.FamilyMemberHistory;
import ca.uhn.fhir.model.dstu2.resource.FamilyMemberHistory.Condition;
import ca.uhn.fhir.model.dstu2.resource.RiskAssessment;
import ca.uhn.fhir.model.dstu2.resource.RiskAssessment.Prediction;
import ca.uhn.fhir.model.dstu2.valueset.AdministrativeGenderEnum;
import ca.uhn.fhir.model.dstu2.valueset.BundleTypeEnum;
import ca.uhn.fhir.model.dstu2.valueset.HTTPVerbEnum;
import ca.uhn.fhir.model.primitive.CodeDt;
import ca.uhn.fhir.model.primitive.IdDt;
import ca.uhn.fhir.model.primitive.StringDt;
import ca.uhn.fhir.rest.api.MethodOutcome;
import ca.uhn.fhir.rest.client.IGenericClient;
import com.desiree.model.HistoryRiskFactors;
import com.desiree.model.MyPatient;
import com.desiree.model.PatientHistory3;

@SuppressWarnings("rawtypes")
public class HapiFhir extends GenericForwardComposer {

	private static final long serialVersionUID = 1L;

	private Component comp;
	
	private FhirContext ctx;
	private String serverBase;
	private IGenericClient client;
	
	private Grid grid3, gridRiskFactors;
	private Rows rows3, rowsRiskFactors;
	private Textbox tbXML, tbJSON;
	private Radiogroup rgSex;
	private Datebox dbBirthdate;
	private Textbox txtName, txtFamily, txtAssignedArea, txtSocioEconomicalStatus;
	
	
	@SuppressWarnings("unchecked")
	public void doAfterCompose(org.zkoss.zk.ui.Component comp) throws Exception {
		
		super.doAfterCompose(comp);

		this.comp = comp;
		
		
		//Crete the FHIR client
		ctx = FhirContext.forDstu2();
//		ctx.registerCustomType(ScenarioACustomResource.class);
//		ctx.registerCustomType(FamilyHistoryDataType.class);
		
		serverBase = "http://localhost:8080/HapiFhirServer/fhir/";
		client = ctx.newRestfulGenericClient(serverBase);
	}
	
	
	public void onClick$btnBundle(){
		
		System.out.println();
		System.out.println("Bundle button fire");
		
		// Create a patient object
		MyPatient patient = new MyPatient();
		patient.addIdentifier()
		   .setSystem("http://desiree-project.eu/Desims")
		   .setValue("DESIREE_001");
		
		if(!txtFamily.getValue().isEmpty() && !txtName.getValue().isEmpty()){
			patient.addName()
			   .addFamily(txtFamily.getValue())
			   .addGiven(txtName.getValue());
		}else patient.addName()
		   .addFamily("Simpson")
		   .addGiven("J")
		   .addGiven("Homer");
		
		if(rgSex.getSelectedIndex()==0)
			patient.setGender(AdministrativeGenderEnum.FEMALE);
		else if(rgSex.getSelectedIndex()==1)
			patient.setGender(AdministrativeGenderEnum.MALE);
//		patient.setBirthDate(new DateDt(dbBirthdate.getValue()));
		
		if(txtAssignedArea.getValue()!=null && !txtAssignedArea.getValue().isEmpty())
			patient.setAssignedArea(new CodeDt(txtAssignedArea.getValue()));
		if(txtSocioEconomicalStatus.getValue()!=null && !txtSocioEconomicalStatus.getValue().isEmpty())
			patient.setSocioeconomicalStatus(new CodeDt(txtSocioEconomicalStatus.getValue()));
		 
		// Give the patient a temporary UUID so that other resources in
		// the transaction can refer to it
//		patient.setId(IdDt.newRandomUuid());
		patient.setId(new IdDt("DESIREE_001"));
		
		
		//Create FamilyMemberHistory resource
		List<FamilyMemberHistory> listafamily = new ArrayList<FamilyMemberHistory>();
		FamilyMemberHistory family;
		StringDt age;
		Iterator ite = rows3.getChildren().iterator();
		while(ite.hasNext()) { //Itera para cada Row
	    	family = new FamilyMemberHistory();
			Component c = (Component) ite.next(); 
	    	Row row=(Row)c;
	    	List<Condition> conditionList = new ArrayList<Condition>();
	    	Condition condition;
	    	Iterator ite1 = row.getChildren().iterator();
	    	while(ite1.hasNext()){ //Itera los elementos de dentro de la Row
	    		Component c1 = (Component) ite1.next();
	    		if(c1.getId().startsWith("lblLocationHistory3_")){
		    		condition = new Condition();
		    		condition.setCode(new CodeableConceptDt().setText(((Label)c1).getValue()));
		    		conditionList.add(condition);
		    		family.setCondition(conditionList);
		    	}else if(c1.getId().startsWith("lblAgeHistory3_")){
		    		age = new StringDt(((Label)c1).getValue());
		    		family.setAge(age);
		    	}else if(c1.getId().startsWith("lblFamily3_")){
		    		family.setRelationship(new CodeableConceptDt().setText(((Label)c1).getValue()));
		    	}
	    		family.setPatient(new ResourceReferenceDt(patient.getId().getValue()));
	    	}
	    	listafamily.add(family);
	    }
	    
		//Create RiskAssessment resource
		RiskAssessment risk = null;
		if(rowsRiskFactors.getChildren().size()>0){
			risk = new RiskAssessment();
			List<Prediction> predictionList = new ArrayList<Prediction>();
			Prediction prediction;
			Row fila;
			for(Component aux: rowsRiskFactors.getChildren()){
				fila = new Row();
				fila = (Row) aux;
				Label lbl = (Label) fila.getFirstChild();
				
				prediction = new Prediction();
				
				if(lbl.getValue().toUpperCase().equals("BRCA1")){
					List<CodingDt> listaCodes = new ArrayList<CodingDt>();
					CodingDt code1 = new CodingDt();
					code1.setCode("SNOMED-CT");
					code1.setDisplay("405827002");
					listaCodes.add(code1);
					CodingDt code2 = new CodingDt();
					code2.setCode("NCI");
					code2.setDisplay("C17965");
					listaCodes.add(code2);
					prediction.setOutcome(new CodeableConceptDt().setText(lbl.getValue()).setCoding(listaCodes));
				}else{
					prediction.setOutcome(new CodeableConceptDt().setText(lbl.getValue()));
				}
				predictionList.add(prediction);
			}
			risk.setPrediction(predictionList);
			risk.setSubject(new ResourceReferenceDt(patient.getId().getValue()));
		}

		
		// Create a bundle that will be used as a transaction
		Bundle bundle = new Bundle();
		bundle.setType(BundleTypeEnum.TRANSACTION);
		 
		// Add the patient as an entry. This entry is a POST with an
		// If-None-Exist header (conditional create) meaning that it
		// will only be created if there isn't already a Patient with
		// the identifier DESXXX
		bundle.addEntry()
		   .setFullUrl(patient.getId().getValue())
		   .setResource(patient)
		   .getRequest()
		      .setUrl("Patient")
		      .setIfNoneExist("Patient?identifier=http://desiree-project.eu/Desims|DESXXX")
		      .setMethod(HTTPVerbEnum.POST);
		 
		//Add family member history
		for(FamilyMemberHistory fam: listafamily){
			bundle.addEntry()
			   .setResource(fam)
			   .getRequest()
			      .setUrl("FamilyMemberHistory")
			      .setMethod(HTTPVerbEnum.POST);
		}
		
		//Add risk factors
		if(risk!=null){
			bundle.addEntry()
			   .setResource(risk)
			   .getRequest()
			      .setUrl("RiskAssessment")
			      .setMethod(HTTPVerbEnum.POST);
		}
		
		
		// Log the request
		System.out.println();
//		System.out.println(ctx.newXmlParser().setPrettyPrint(true).encodeResourceToString(bundle));
		tbXML.setValue(ctx.newXmlParser().setPrettyPrint(true).encodeResourceToString(bundle));
		System.out.println();
//		System.out.println(ctx.newJsonParser().setPrettyPrint(true).encodeResourceToString(bundle));
		tbJSON.setValue(ctx.newJsonParser().setPrettyPrint(true).encodeResourceToString(bundle));
		System.out.println();
		
		// Log the response
//		System.out.println(ctx.newXmlParser().setPrettyPrint(true).encodeResourceToString(resp));
		
		MethodOutcome outcome = client.create()
				   .resource(bundle)
				   .prettyPrint()
				   .encodedJson()
				   .execute();
		
		if(outcome!=null){
			
		}
		
	}
	
	
	
	
	/**
	 * *************************************************************************************
	 * FUNCIONES PARA CONTROLAR LOS ELEMENTOS DE LA UI
	 * *************************************************************************************
	 */
	
	public void onClick$btnEditHistory3(){
		//cargar mapa
		Map<Integer, Object> map=new HashMap<Integer,Object>();
		map.put(0,"modal3");
		int i=1;
		Iterator ite = rows3.getChildren().iterator();
	    while(ite.hasNext()) { //Itera para cada Row
	    	Component c = (Component) ite.next(); 
	    	PatientHistory3 ph3= new PatientHistory3();
	    	Row row=(Row)c;
	    	Iterator ite1 = row.getChildren().iterator();
	    	while(ite1.hasNext()){ //Itera los elementos de dentro de la Row
	    		Component c1 = (Component) ite1.next();
	    		if(c1.getId().startsWith("lblLocationHistory3_")){
		    		ph3.setCancerLocation(((Label)c1).getValue());
		    	}else if(c1.getId().startsWith("lblAgeHistory3_")){
		    		ph3.setCancerAge(Integer.parseInt(((Label)c1).getValue()));
		    	}else if(c1.getId().startsWith("lblFamily3_")){
		    		ph3.setCancerRelative(((Label)c1).getValue());
		    	}
	    		map.put(i, ph3);
	    	}
	    	i++;
	    }
		Window window = (Window)Executions.createComponents("history_modal.zul", comp, map);	
		window.addEventListener("onClose",new EventListener<Event>() {
			
			public void onEvent(Event event){
				// Evento al salir de la ventana modal
				Map<Integer, Object> mapa= (Map<Integer, Object>)event.getData();
				actualizarHistory3(mapa);
			}
		});
		window.setClosable(true);
	    window.doModal();
	}
	
	
	/**
	 * M�todo al que se llama al cerrar la modal3, actualiza el grid3
	 * @param mapa
	 */
	private void actualizarHistory3(Map<Integer, Object> mapa){
		//Borra el contenido del grid
		List<Component> toRemove = new Vector<Component>();
		Iterator ite = rows3.getChildren().iterator();
		while (ite.hasNext()){
			Component c=(Component)ite.next();
			toRemove.add(c);
		}
		 for (Component c : toRemove)
             c.detach();
		
		if(mapa.isEmpty()){
			grid3.setVisible(false);
		}else{
			
			//Genera el grid
			grid3.setVisible(true);
			
			Iterator entries = mapa.entrySet().iterator();
			int i=0;
			while (entries.hasNext()) {
			    Map.Entry entry = (Map.Entry) entries.next();
			    PatientHistory3 value = (PatientHistory3)entry.getValue();
			
				Row row=new Row();
				row.setClass("sinFondo");
	
				Label lbl=new Label(value.getCancerLocation());
				lbl.setId("lblLocationHistory3_"+i);					
				row.appendChild(lbl);
	
				Label lbl1=new Label(value.getCancerRelative());
				lbl1.setId("lblFamily3_"+i);
				row.appendChild(lbl1);
				
				Label lblAge=new Label(String.valueOf(value.getCancerAge()));
				lblAge.setId("lblAgeHistory3_"+i);
				row.appendChild(lblAge);
				
				comp.getFellow("rows3").appendChild(row);
				i++;
			}
		}		
	}	
	
	/**
	 * M�todo al que se llama al cerrar la modal risk factors, actualiza los risk factors
	 * @param mapa
	 */
	private void actualizarRiskFactors(Map<Integer, HistoryRiskFactors> mapa){
		
		
		//Borra el contenido del grid
		List<Component> toRemove = new Vector<Component>();
		Iterator ite = rowsRiskFactors.getChildren().iterator();
		while (ite.hasNext()){
			Component c=(Component)ite.next();
			toRemove.add(c);
		}
		 for (Component c : toRemove)
             c.detach();
				 
		if(mapa.isEmpty()){
			gridRiskFactors.setVisible(false);

		}else{
			HistoryRiskFactors hrf=new HistoryRiskFactors();
			hrf=mapa.get(0);
			gridRiskFactors.setVisible(true);
			mostrarCheckbox(hrf.getBrca1(),"Brca1");			
			mostrarCheckbox(hrf.getBrca2(),"Brca2");
			mostrarCheckbox(hrf.getAtypicalHiperplaxia(),"Atypical hiperplaxia");
			mostrarCheckbox(hrf.getPalb2(),"Palb2");
			mostrarCheckbox(hrf.getRad51c(),"Rad51c");
			mostrarCheckbox(hrf.getRad51d(),"Rad51d");
			mostrarCheckbox(hrf.getP53(),"P53Y");
			mostrarCheckbox(hrf.getPten(),"Pten");
			mostrarCheckbox(hrf.getStk11(),"Stk11");
			mostrarCheckbox(hrf.getIhc(),"Ihc");			
		}
	}
	
	private void mostrarCheckbox(Boolean valor, String texto){
		
		if(valor!=null){
			Row row=new Row();
			row.setClass("sinFondo");
			Label lbl=new Label(texto);
			lbl.setClass("etiquetasL16");
			row.appendChild(lbl);
			
			Label lblY=new Label();
			lblY.setId(texto.toLowerCase());
			if(valor) lblY.setValue("Yes");
			else lblY.setValue("No");
			row.appendChild(lblY);
			rowsRiskFactors.appendChild(row);
		}
	}
	
	/**
	 * M�todo que llama a la modal history_risk_factors_modal.zul
	 * 
	 */
	public void onClick$btnOtherRiskFactors(){
		//cargar mapa
		Map<Integer, HistoryRiskFactors> map=new HashMap<Integer,HistoryRiskFactors>();
		HistoryRiskFactors hrf= new HistoryRiskFactors();
		if(gridRiskFactors.isVisible()){
			if(null!=comp.getFellowIfAny("brca1")){
		    	if("Yes".equalsIgnoreCase(((Label) comp.getFellowIfAny("brca1")).getValue())){
		    		hrf.setBrca1(true);
		    	}
		    	if("No".equalsIgnoreCase(((Label) comp.getFellowIfAny("brca1")).getValue())){
		    		hrf.setBrca1(false);
		    	}
		    }
			if(null!=comp.getFellowIfAny("brca2")){
		    	if("Yes".equalsIgnoreCase(((Label) comp.getFellowIfAny("brca2")).getValue())){
		    		hrf.setBrca2(true);
		    	}
		    	if("No".equalsIgnoreCase(((Label) comp.getFellowIfAny("brca2")).getValue())){
		    		hrf.setBrca2(false);
		    	}
		    }
			if(null!=comp.getFellowIfAny("atypical hiperplaxia")){
		    	if("Yes".equalsIgnoreCase(((Label) comp.getFellowIfAny("atypical hiperplaxia")).getValue())){
		    		hrf.setAtypicalHiperplaxia(true);
		    	}
		    	if("No".equalsIgnoreCase(((Label) comp.getFellowIfAny("atypical hiperplaxia")).getValue())){
		    		hrf.setAtypicalHiperplaxia(false);
		    	}
		    }			
			if(null!=comp.getFellowIfAny("palb2")){
		    	if("Yes".equalsIgnoreCase(((Label) comp.getFellowIfAny("palb2")).getValue())){
		    		hrf.setPalb2(true);
		    	}
		    	if("No".equalsIgnoreCase(((Label) comp.getFellowIfAny("palb2")).getValue())){
		    		hrf.setPalb2(false);
		    	}
		    }			
			if(null!=comp.getFellowIfAny("rad51c")){
		    	if("Yes".equalsIgnoreCase(((Label) comp.getFellowIfAny("rad51c")).getValue())){
		    		hrf.setRad51c(true);
		    	}
		    	if("No".equalsIgnoreCase(((Label) comp.getFellowIfAny("rad51c")).getValue())){
		    		hrf.setRad51c(false);
		    	}
		    }			
			if(null!=comp.getFellowIfAny("rad51d")){
		    	if("Yes".equalsIgnoreCase(((Label) comp.getFellowIfAny("rad51d")).getValue())){
		    		hrf.setRad51d(true);
		    	}
		    	if("No".equalsIgnoreCase(((Label) comp.getFellowIfAny("rad51d")).getValue())){
		    		hrf.setRad51d(false);
		    	}
		    }					
			if(null!=comp.getFellowIfAny("p53")){
		    	if("Yes".equalsIgnoreCase(((Label) comp.getFellowIfAny("p53")).getValue())){
		    		hrf.setP53(true);
		    	}
		    	if("No".equalsIgnoreCase(((Label) comp.getFellowIfAny("p53")).getValue())){
		    		hrf.setP53(false);
		    	}
		    }					
			if(null!=comp.getFellowIfAny("pten")){
		    	if("Yes".equalsIgnoreCase(((Label) comp.getFellowIfAny("pten")).getValue())){
		    		hrf.setPten(true);
		    	}
		    	if("No".equalsIgnoreCase(((Label) comp.getFellowIfAny("pten")).getValue())){
		    		hrf.setPten(false);
		    	}
		    }			
			if(null!=comp.getFellowIfAny("stk11")){
		    	if("Yes".equalsIgnoreCase(((Label) comp.getFellowIfAny("stk11")).getValue())){
		    		hrf.setStk11(true);
		    	}
		    	if("No".equalsIgnoreCase(((Label) comp.getFellowIfAny("stk11")).getValue())){
		    		hrf.setStk11(false);
		    	}
		    }			
			if(null!=comp.getFellowIfAny("ihc")){
		    	if("Yes".equalsIgnoreCase(((Label) comp.getFellowIfAny("ihc")).getValue())){
		    		hrf.setIhc(true);
		    	}
		    	if("No".equalsIgnoreCase(((Label) comp.getFellowIfAny("ihc")).getValue())){
		    		hrf.setIhc(false);
		    	}
		    }			
						
		    map.put(0, hrf);
		}
		
		

		Window window = (Window)Executions.createComponents("history_risk_factors_modal.zul", comp, map);	
		window.addEventListener("onClose",new EventListener<Event>() {
			
			public void onEvent(Event event){
				// Evento al salir de la ventana modal
				Map<Integer, HistoryRiskFactors> mapa= (Map<Integer, HistoryRiskFactors>)event.getData();
				actualizarRiskFactors(mapa);
			}
		});
		window.setClosable(true);
	    window.doModal();		
	}
}
