package com.desiree.model;

public class PatientAllergies {

	
	private String allergy;
	private int monthsYears;
	private String unidad;
	
	public PatientAllergies() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getAllergy() {
		return allergy;
	}

	public void setAllergy(String allergy) {
		this.allergy = allergy;
	}

	public int getMonthsYears() {
		return monthsYears;
	}

	public void setMonthsYears(int monthsYears) {
		this.monthsYears = monthsYears;
	}

	public String getUnidad() {
		return unidad;
	}

	public void setUnidad(String unidad) {
		this.unidad = unidad;
	}

	public PatientAllergies(String allergy, int monthsYears, String unidad) {
		super();
		this.allergy = allergy;
		this.monthsYears = monthsYears;
		this.unidad=unidad;
	}

	@Override
	public String toString() {
		return "PatientAllergies [allergy=" + allergy + ", monthsYears=" + monthsYears + ", unidad=" + unidad + "]";
	}


}
