package com.desiree.model;

public class PatientHistory3 {

	
	private String cancerLocation;
	private String cancerRelative;
	private int cancerAge;
	public PatientHistory3() {
		super();
		// TODO Auto-generated constructor stub
	}
	public PatientHistory3(String cancerLocation, String cancerRelative, int cancerAge) {
		super();
		this.cancerLocation = cancerLocation;
		this.cancerRelative=cancerRelative;
		this.cancerAge = cancerAge;
	}
	public String getCancerLocation() {
		return cancerLocation;
	}
	public void setCancerLocation(String cancerLocation) {
		this.cancerLocation = cancerLocation;
	}
	public String getCancerRelative() {
		return cancerRelative;
	}
	public void setCancerRelative(String cancerRelative) {
		this.cancerRelative = cancerRelative;
	}
	public int getCancerAge() {
		return cancerAge;
	}
	public void setCancerAge(int cancerAge) {
		this.cancerAge = cancerAge;
	}
	@Override
	public String toString() {
		return "PatientHistory3 [cancerLocation=" + cancerLocation + ", cancerRelative=" + cancerRelative
				+ ", cancerAge=" + cancerAge + "]";
	}
}