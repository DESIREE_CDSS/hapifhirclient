package com.desiree.model;

public class HistoryRiskFactors {

	
	private Boolean brca1;
	private Boolean brca2;
	private Boolean atypicalHiperplaxia;
	private Boolean palb2; 
	private Boolean rad51c;
	private Boolean rad51d;
	private Boolean p53;
	private Boolean pten;
	private Boolean stk11;
	private Boolean ihc;
	
	public HistoryRiskFactors() {
		super();
		brca1=null;
		brca2=null;
		atypicalHiperplaxia=null;
		palb2=null; 
		rad51c=null;
		rad51d=null;
		p53=null;
		pten=null;
		stk11=null;
		ihc=null;
	}

	public Boolean getBrca1() {
		return brca1;
	}

	public void setBrca1(Boolean brca1) {
		this.brca1 = brca1;
	}

	public Boolean getBrca2() {
		return brca2;
	}

	public void setBrca2(Boolean brca2) {
		this.brca2 = brca2;
	}

	public Boolean getAtypicalHiperplaxia() {
		return atypicalHiperplaxia;
	}

	public void setAtypicalHiperplaxia(Boolean atypicalHiperplaxia) {
		this.atypicalHiperplaxia = atypicalHiperplaxia;
	}

	public Boolean getPalb2() {
		return palb2;
	}

	public void setPalb2(Boolean palb2) {
		this.palb2 = palb2;
	}

	public Boolean getRad51c() {
		return rad51c;
	}

	public void setRad51c(Boolean rad51c) {
		this.rad51c = rad51c;
	}

	public Boolean getRad51d() {
		return rad51d;
	}

	public void setRad51d(Boolean rad51d) {
		this.rad51d = rad51d;
	}

	public Boolean getP53() {
		return p53;
	}

	public void setP53(Boolean p53) {
		this.p53 = p53;
	}

	public Boolean getPten() {
		return pten;
	}

	public void setPten(Boolean pten) {
		this.pten = pten;
	}

	public Boolean getStk11() {
		return stk11;
	}

	public void setStk11(Boolean stk11) {
		this.stk11 = stk11;
	}

	public Boolean getIhc() {
		return ihc;
	}

	public void setIhc(Boolean ihc) {
		this.ihc = ihc;
	}

	@Override
	public String toString() {
		return "HistoryRiskFactors [brca1=" + brca1 + ", brca2=" + brca2 + ", atypicalHiperplaxia="
				+ atypicalHiperplaxia + ", palb2=" + palb2 + ", rad51c=" + rad51c + ", rad51d=" + rad51d + ", p53="
				+ p53 + ", pten=" + pten + ", stk11=" + stk11 + ", ihc=" + ihc + "]";
	}

	
}
	